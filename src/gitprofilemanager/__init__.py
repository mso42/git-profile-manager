import subprocess
import os

from .apply import parse_config


def do_list(args):
    try:
        with os.scandir(args.profiledir) as profile_directory:
            for entry in profile_directory:
                if (entry.is_file()):
                    print(entry.name)
    except FileNotFoundError:
        pass


def apply(args):
    profile_path = os.path.join(args.profiledir, args.profile)
    try:
        with open(profile_path, 'r') as profile_file:
            for (key, val) in parse_config(profile_file):
                git_config('--local', key, val)
    except FileNotFoundError:
        print("Profile {} not found".format(args.profile))


def git_config(*args):
    print('Running git config {}'.format(' '.join(args)))
    subprocess.run(['git', 'config', *args])
