class ConfigParseError(Exception):
    pass

def parse_config(config_lineiterator):
    for line in config_lineiterator:
        if line[0] == '#':
            continue
        without_newline = line[:-1]
        splits = without_newline.split('=', maxsplit=1)
        if len(splits) < 2:
            raise ConfigParseError()
        [key, val] = splits
        key = key.rstrip()
        val = val.lstrip()
        yield (key, val)

